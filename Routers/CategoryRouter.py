from fastapi import APIRouter

from Services.CategoryService import add_category, delete_category, get_all_category, change_category

api_router = APIRouter()


@api_router.get("/get_category", tags=["Category"])
async def get_category_image():
    return await get_all_category()


@api_router.post("/add_category/{name_category}", tags=["Category"])
async def add_category_image(name_category: str):
     return await add_category(name_category)


@api_router.post("/change_category/{name_category}&{new_name_category}", tags=["Category"])
async def change_category_image(name_category: str, new_name_category: str):
    return await change_category(name_category, new_name_category)


@api_router.delete("/delete_category/{name_category}", tags=["Category"])
async def delete_category_image(name_category: str):
    return await delete_category(name_category)
