import os.path

from fastapi import APIRouter, UploadFile

from Services.ImageService import (add_image, delete_image, get_information_image,
                                   get_information_id, download_image_id, delete_image_full)

api_router = APIRouter()


@api_router.post("/upload_image&category", tags=["Images"])
async def upload_image(file: UploadFile, category: str):
    return await add_image(file, category)


@api_router.get("/download_image/{id}", tags=["Images"])
async def download_image(hash_id: str):
    return await download_image_id(hash_id)


@api_router.get("/get_information", tags=["Images"])
async def get_all_information_image():
    return await get_information_image()


@api_router.get("/get_information/{id}", tags=["Images"])
async def get_information(hash_id: str):
    return await get_information_id(hash_id)


@api_router.delete("/delete_image/{HashId}", tags=["Images"])
async def delete(hash_id: str):
    return await delete_image(hash_id)


@api_router.delete("/delete_image_full/{HashId}", tags=["Images"])
async def delete(hash_id: str):
    return await delete_image_full(hash_id)
