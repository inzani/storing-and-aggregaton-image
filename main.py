import uvicorn

from fastapi import FastAPI
from Routers.ImageRouter import api_router as image_router
from Routers.CategoryRouter import api_router as category_router


app = FastAPI()

app.include_router(image_router, prefix="/images")
app.include_router(category_router, prefix="/category")

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8001)
