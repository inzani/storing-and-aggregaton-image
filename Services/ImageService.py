import os
import dhash
import asyncio

from io import BytesIO
from fastapi import UploadFile
from PIL import Image, ImageOps
from sqlalchemy import select, delete
from fastapi.responses import FileResponse

from Storage.DbStorage import SessionLocal
from Storage.Table.FileImage import FileImage


async def __request_database(query: any):
    async with SessionLocal() as db:
        data = await db.execute(query)
        return data


async def __check_image(hash_image: str):
    query = select(FileImage).where(FileImage.hash_id == hash_image)
    category = await __request_database(query)
    fetch = category.fetchall()
    if fetch:
        return True
    return False


def __convert_to_json(images):
    all_image = []
    images = images.fetchall()
    for image in images:
        image_data = {
            'id': str(image.FileImage.id),
            'hash_id': image.FileImage.hash_id,
            'name': image.FileImage.name,
            'path': image.FileImage.path,
            'expansion': image.FileImage.expansion,
            'category': image.FileImage.category,
            'size': image.FileImage.size,
            'width': image.FileImage.width,
            'height': image.FileImage.height,
        }
        all_image.append(image_data)
    return all_image


async def __check_format_file(file_name: any):
    name, extension = os.path.splitext(file_name)
    if extension == ".jpg" or extension == ".jpeg":
        return True
    return False


async def __save_image(file_read: any, file_name: str):
    file_name = f"ImagesStorage\\{file_name}"
    with open(file_name, "wb") as file:
        print(f"len {len(file_read)}")
        file.write(file_read)


async def __async_resize(image, size):
    return await asyncio.to_thread(ImageOps.fit, image, size, Image.LANCZOS)


async def __hash_image(image_data: any):
    with Image.open(BytesIO(image_data)) as image:
        row, col = dhash.dhash_row_col(image)
        return dhash.format_hex(row, col)


async def add_image(file_image: UploadFile, category: str):
    file_name = file_image.filename
    file_read = file_image.file.read()
    check_format = await __check_format_file(file_name)
    if not check_format:
        print("Incorrect format")
        raise Exception(f"Incorrect format {file_name}")
    hash_id = await __hash_image(file_read)
    hash_for_database = await __check_image(hash_id)
    if hash_for_database:
        print("Such an image is already in the database")
        raise Exception(f"Such an image is already in the database {file_name}")
    await __save_image(file_read, file_name)
    path = f"ImagesStorage\\{file_name}"
    name, extension = os.path.splitext(file_name)
    image = Image.open(file_image.file)
    width, height = image.size
    size = os.path.getsize(path)

    image = FileImage(
        hash_id=hash_id,
        name=name,
        path=path,
        expansion=extension,
        category=category,
        size=size,
        width=width,
        height=height,
    )
    async with SessionLocal() as db:
        db.add(image)
        await db.commit()
    result_add_image = await __check_image(hash_id)
    if result_add_image:
        return {"status": 200, "hash_image": hash_id, }
    raise Exception(f"Error adding file: {file_image.filename}")


async def delete_image(hash_id: str):
    if await __check_image(hash_id):
        async with SessionLocal() as db:
            select_query = select(FileImage.path).where(FileImage.hash_id == hash_id)
            data = await db.execute(select_query)
            data = data.first()
            if data is None:
                raise FileNotFoundError(f"The file was not found in the database: {hash_id}")
            data = data[0]
            is_file = os.path.isfile(data)
            if not is_file:
                raise FileNotFoundError(f"The specified file cannot be found: {data}")
            os.remove(data)
            is_file = os.path.isfile(data)
        if not is_file:
            return {"status": 200, "message": f"file: {hash_id} successfully deleted for disk"}
        raise Exception(f"failed to delete file from  {hash_id}")
    else:
        raise Exception(f"The file is not in the database: {hash_id}")


async def delete_image_full(hash_id: str):
    if await __check_image(hash_id):
        async with SessionLocal() as db:
            select_query = select(FileImage.path).where(FileImage.hash_id == hash_id)
            data = await db.execute(select_query)
            data = data.first()
            if data is not None:
                data = data[0]
                is_file = os.path.isfile(data)
                if is_file:
                    os.remove(data)
                is_file = os.path.isfile(data)
            delete_query = delete(FileImage).where(FileImage.hash_id == hash_id)
            await db.execute(delete_query)
            await db.commit()
            check_delete_database = await __check_image(hash_id)
            if check_delete_database:
                raise Exception(f"Error delete file {hash_id}, delete:{check_delete_database},"
                                f"delete disk: {is_file}")
            return {"status": 200, "message": f"file: {hash_id} successfully deleted full"}
    else:
        raise Exception(f"The file is not in the database: {hash_id}")


async def get_information_image():
    query = select(FileImage)
    all_image = await __request_database(query)
    convert = __convert_to_json(all_image)
    return convert


async def get_information_id(hashid: str):
    query = select(FileImage).where(FileImage.hash_id == hashid)
    id_image = await __request_database(query)
    convert = __convert_to_json(id_image)
    return convert


async def download_image_id(hashid: str):
    query = select(FileImage.path).where(FileImage.hash_id == hashid)
    image = await __request_database(query)
    image = image.scalar()
    is_file = os.path.isfile(image)
    if is_file:
        return FileResponse(image)
    raise FileNotFoundError("File not found for disk")
