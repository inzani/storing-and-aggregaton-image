import aiofiles
import datetime as dt


async def write_to_file(data):
    try:
        async with aiofiles.open("log.txt", 'a') as file:
            await file.write(f"{dt.datetime.now()} {str(data)}\n")
    except Exception as e:
        print(f"Произошла ошибка при записи в файл: {str(e)}")
