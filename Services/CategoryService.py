from sqlalchemy import select, delete, update, insert

from Storage.DbStorage import SessionLocal
from Storage.Table.CategoryImage import CategoryImage


def __convert_to_json(all_category):
    categories = []
    for category in all_category:
        category_dict = {
            'NameCategory': category.CategoryImage.NameCategory,
        }
        categories.append(category_dict)
    return categories


async def __check_category(name_category: str):
    async with SessionLocal() as db:
        query = select(CategoryImage).where(CategoryImage.NameCategory == name_category)
        category = await db.execute(query)
        fetch = category.fetchall()
        if fetch:
            return True
        return False


async def add_category(name_category: str):
    check_category = await __check_category(name_category)
    if not check_category:
        async with SessionLocal() as db:
            query = insert(CategoryImage).values(NameCategory=name_category)
            await db.execute(query)
            await db.commit()
            await __check_category(name_category)
        return {"message": "record successfully added"}
    else:
        raise Exception(f"Error adding {name_category} to the database")


async def delete_category(name_category: str):
    check_category = await __check_category(name_category)
    if check_category:
        async with SessionLocal() as db:
            query = delete(CategoryImage).where(CategoryImage.NameCategory == name_category)
            await db.execute(query)
            await db.commit()
            await __check_category(name_category)
            return {"message": f"{name_category} successfully deleted"}
    else:
        raise Exception(f"Error deleted {name_category} to the database")


async def get_all_category():
    async with SessionLocal() as db:
        query = select(CategoryImage)
        all_category = await db.execute(query)
        result_convert = __convert_to_json(all_category)
        return result_convert


async def change_category(old_name_category: str, new_name_category: str):
    check_old_category = await __check_category(old_name_category)
    check_new_category = await __check_category(new_name_category)

    if not check_old_category:
        return False
    if check_new_category:
        return False

    async with SessionLocal() as db:
        query = update(CategoryImage).where(CategoryImage.NameCategory == old_name_category).values(
            NameCategory=new_name_category)
        await db.execute(query)
        await db.commit()

        check_change = await __check_category(new_name_category)
        return check_change
