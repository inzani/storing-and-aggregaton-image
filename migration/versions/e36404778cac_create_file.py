"""create_file

Revision ID: e36404778cac
Revises: 2d12ab1a2cb9
Create Date: 2023-10-11 22:06:41.893341

"""
from typing import Sequence, Union
from sqlalchemy.dialects.postgresql import UUID

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'e36404778cac'
down_revision: Union[str, None] = '2d12ab1a2cb9'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.create_table(
        'FileImage',
        sa.Column('id', UUID(as_uuid=True), primary_key=True, unique=True, nullable=False),
        sa.Column('hash_id', sa.String(), index=True),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('path', sa.String(), nullable=False),
        sa.Column('expansion', sa.String()),
        sa.Column('category', sa.String()),
        sa.Column('size', sa.Integer()),
        sa.Column('width', sa.Integer()),
        sa.Column('height', sa.Integer())
    )

    op.create_table(
        'CategoryImage',
        sa.Column('NameCategory', sa.String(), primary_key=True, index=True, nullable=False)
    )


def downgrade() -> None:
    op.drop_table('FileImage')
    op.drop_table('CategoryImage')
