import uuid

from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class FileImage(Base):
    __tablename__ = 'FileImage'

    id = Column(UUID(as_uuid=True), primary_key=True, unique=True, index=True, default=uuid.uuid4)
    hash_id = Column(String, index=True)
    name = Column(String)
    path = Column(String)
    expansion = Column(String)
    category = Column(String)
    size = Column(Integer)
    width = Column(Integer)
    height = Column(Integer)

