from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class CategoryImage(Base):
    __tablename__ = 'CategoryImage'

    NameCategory = Column(String, primary_key=True, index=True)
