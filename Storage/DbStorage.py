import asyncio
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker


from Storage.Table.CategoryImage import Base as BaseCategory
from Storage.Table.FileImage import Base as BaseImage

db_url = 'postgresql+asyncpg://postgres:admin@localhost:5432/postgres'

engine = create_async_engine(db_url, echo=True)
Base = declarative_base()

SessionLocal = sessionmaker(bind=engine, class_=AsyncSession, expire_on_commit=False)


async def startup_db():
    async with engine.begin() as conn:
        await conn.run_sync(BaseCategory.metadata.create_all)

    async with engine.begin() as conn:
        await conn.run_sync(BaseImage.metadata.create_all)

if __name__ == "__main__":
    asyncio.run(startup_db())
