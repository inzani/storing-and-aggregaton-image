import asyncio
import uuid

from unittest import TestCase, mock, IsolatedAsyncioTestCase
from unittest.mock import Mock, patch

from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import UUID

from Services.ImageService import SessionLocal, get_information_image
#from Storage.Table.FileImage import FileImage

Base = declarative_base()


class FileImage(Base):
    __tablename__ = 'FileImage'
    id = Column(UUID(as_uuid=True), primary_key=True, unique=True, index=True, default=uuid.uuid4)
    hash_id = Column(String)
    name = Column(String)
    path = Column(String)
    expansion = Column(String)
    category = Column(String)
    size = Column(Integer)
    width = Column(Integer)
    height = Column(Integer)

class TestGetAllImages(IsolatedAsyncioTestCase):
    # def test_get_information_id(self):
    #     sessionmaker_mock = Mock()
    #     session_mock = sessionmaker_mock()
    #     query_mock = session_mock.query()
    #
    #     image_one = FileImage(
    #         id="67795385-5d8d-4464-ad10-635b1d41d37f",
    #         hash_id="d392c399151d95b301b6fe80237bfd86",
    #         name="photo_2023-09-21_13-10-55",
    #         path="ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
    #         expansion=".jpg",
    #         category="Автомобиль",
    #         size=265887,
    #         width=1280,
    #         height=577,
    #     )
    #
    #     image_json_one = [{
    #         "id": "67795385-5d8d-4464-ad10-635b1d41d37f",
    #         "hash_id": "d392c399151d95b301b6fe80237bfd86",
    #         "name": "photo_2023-09-21_13-10-55",
    #         "path": "ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
    #         "expansion": ".jpg",
    #         "category": "Автомобиль",
    #         "size": 265887,
    #         "width": 1280,
    #         "height": 577,
    #     }]
    #     images = [image_one]
    #
    #     query_mock.all.return_value = images
    #
    #     with mock.patch('Storage.DbStorage.SessionLocal', sessionmaker_mock):
    #         result_image_one = asyncio.run(get_information_id("d392c399151d95b301b6fe80237bfd86"))
    #
    #     self.assertEqual(result_image_one, image_json_one)
    #
    #     sessionmaker_mock.assert_called_once()

    # @patch('Services.ImageService.SessionLocal', new_callable=Mock)
    # def test_get_information_all(self, mock_session):
    #     image_one = FileImage(
    #         id="67795385-5d8d-4464-ad10-635b1d41d37f",
    #         hash_id="d392c399151d95b301b6fe80237bfd86",
    #         name="photo_2023-09-21_13-10-55",
    #         path="ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
    #         expansion=".jpg",
    #         category="Автомобиль",
    #         size=265887,
    #         width=1280,
    #         height=577,
    #     )
    #
    #     image_two = FileImage(
    #         id="67795385-5d8d-4464-ad10-635b1d41d37f",
    #         hash_id="d392c399151d95b301b6fe80237bfd86",
    #         name="photo_2023-09-21_13-10-55",
    #         path="ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
    #         expansion=".jpg",
    #         category="Автомобиль",
    #         size=265887,
    #         width=1280,
    #         height=577,
    #     )
    #
    #     images_json = [{
    #         "id": "67795385-5d8d-4464-ad10-635b1d41d37f",
    #         "hash_id": "d392c399151d95b301b6fe80237bfd86",
    #         "name": "photo_2023-09-21_13-10-55",
    #         "path": "ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
    #         "expansion": ".jpg",
    #         "category": "Автомобиль",
    #         "size": 265887,
    #         "width": 1280,
    #         "height": 577,
    #     },
    #     {
    #         "id": "67795385-5d8d-4464-ad10-635b1d41d37f",
    #         "hash_id": "d392c399151d95b301b6fe80237bfd86",
    #         "name": "photo_2023-09-21_13-10-55",
    #         "path": "ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
    #         "expansion": ".jpg",
    #         "category": "Автомобиль",
    #         "size": 265887,
    #         "width": 1280,
    #         "height": 577,
    #     }]
    #     images = [image_one, image_two]
    #
    #     # sessionmaker_mock = Mock()
    #     # session_mock = sessionmaker_mock()
    #     query_mock = mock_session.query()
    #     query_mock.all.return_value = images
    #
    #     #with mock.patch('Storage.DbStorage.SessionLocal', sessionmaker_mock):
    #     result_image_one = asyncio.run(get_information_image())
    #     print(f"result_image_one {result_image_one}")
    #
    #     self.assertEqual(result_image_one, images_json)
    #
    #     self.assert_called_once()

    @patch('Services.ImageService.SessionLocal', new_callable=Mock)
    def test_get_information_all(self, mock_session):
        image_one = FileImage(
            id="67795385-5d8d-4464-ad10-635b1d41d37f",
            hash_id="d392c399151d95b301b6fe80237bfd86",
            name="photo_2023-09-21_13-10-55",
            path="ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
            expansion=".jpg",
            category="Автомобиль",
            size=265887,
            width=1280,
            height=577,
        )

        image_two = FileImage(
            id="67795385-5d8d-4464-ad10-635b1d41d37f",
            hash_id="d392c399151d95b301b6fe80237bfd86",
            name="photo_2023-09-21_13-10-55",
            path="ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
            expansion=".jpg",
            category="Автомобиль",
            size=265887,
            width=1280,
            height=577,
        )

        images_json = [{
            "id": "67795385-5d8d-4464-ad10-635b1d41d37f",
            "hash_id": "d392c399151d95b301b6fe80237bfd86",
            "name": "photo_2023-09-21_13-10-55",
            "path": "ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
            "expansion": ".jpg",
            "category": "Автомобиль",
            "size": 265887,
            "width": 1280,
            "height": 577,
        },
        {
            "id": "67795385-5d8d-4464-ad10-635b1d41d37f",
            "hash_id": "d392c399151d95b301b6fe80237bfd86",
            "name": "photo_2023-09-21_13-10-55",
            "path": "ImagesStorage\\photo_2023-09-21_13-10-55.jpg",
            "expansion": ".jpg",
            "category": "Автомобиль",
            "size": 265887,
            "width": 1280,
            "height": 577,
        }]
        images = [image_one, image_two]

        # sessionmaker_mock = Mock()
        # session_mock = sessionmaker_mock()
        query_mock = mock_session.query()
        query_mock.all.return_value = images

        #with mock.patch('Storage.DbStorage.SessionLocal', sessionmaker_mock):
        result_image_one = asyncio.run(get_information_image())
        print(f"result_image_one {result_image_one}")

        self.assertEqual(result_image_one, images_json)

        self.assert_called_once()